package com.example.hang;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class CategoryActivity extends AppCompatActivity {
    private ImageView imglogo;
    private TextView txtCategory;
    private Button btnAnimal;
    private Button btnCar;
    private Button btnFood;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        imglogo = findViewById(R.id.imglogo);
        txtCategory = findViewById(R.id.txtCategory);
        btnAnimal = findViewById(R.id.btnAnimal);
        btnCar = findViewById(R.id.btnCar);
        btnFood = findViewById(R.id.btnFood);


        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CategoryActivity.this, MainActivity.class);
                switch (v.getId()) {
                    case R.id.btnAnimal:
                        intent.putExtra("category", "animal");
                        break;
                    case R.id.btnCar:
                        intent.putExtra("category", "car");
                        break;
                    case R.id.btnFood:
                        intent.putExtra("category", "food");
                        break;
                }
                startActivity(intent);
                finish();

            }
        };

        btnAnimal.setOnClickListener(onClickListener);
        btnCar.setOnClickListener(onClickListener);
        btnFood.setOnClickListener(onClickListener);

    }


}
