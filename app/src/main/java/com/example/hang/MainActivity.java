package com.example.hang;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private String dash;
    private String dash2;
    private String selecetedWord;
    private String selecetedWord2;
    private TextView txtDash, txtSecondWord;
    private Button btnA;
    private Button btnB;
    private Button btnC;
    private Button btnD;
    private Button btnE;
    private Button btnF;
    private Button btnG;
    private Button btnH;
    private Button btnI;
    private Button btnJ;
    private Button btnK;
    private Button btnL;
    private Button btnM;
    private Button btnN;
    private Button btnO;
    private Button btnP;
    private Button btnQ;
    private Button btnR;
    private Button btnS;
    private Button btnT;
    private Button btnU;
    private Button btnV;
    private Button btnW;
    private Button btnX;
    private Button btnY;
    private Button btnZ;
    private Button btnfinish;
    private Button btnNext;
    private String finalWord = "";

    private String[] food = {"pizza", "steak", "pasta", "chicken"};
    private String[] animal = {"tiger", "camel", "dog", "rat"};
    private String[] car = {"benz", "bmw", "pride", "peykan"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtDash = findViewById(R.id.txtWord);
        txtSecondWord = findViewById(R.id.txtSecondWord);


        btnA = findViewById(R.id.btnA);
        btnB = findViewById(R.id.btnB);
        btnC = findViewById(R.id.btnC);
        btnD = findViewById(R.id.btnD);
        btnE = findViewById(R.id.btnE);
        btnF = findViewById(R.id.btnF);
        btnG = findViewById(R.id.btnG);
        btnH = findViewById(R.id.btnH);
        btnI = findViewById(R.id.btnI);
        btnJ = findViewById(R.id.btnJ);
        btnK = findViewById(R.id.btnK);
        btnL = findViewById(R.id.btnL);
        btnM = findViewById(R.id.btnM);
        btnN = findViewById(R.id.btnN);
        btnO = findViewById(R.id.btnO);
        btnP = findViewById(R.id.btnP);
        btnQ = findViewById(R.id.btnQ);
        btnR = findViewById(R.id.btnR);
        btnS = findViewById(R.id.btnS);
        btnT = findViewById(R.id.btnT);
        btnU = findViewById(R.id.btnU);
        btnV = findViewById(R.id.btnV);
        btnW = findViewById(R.id.btnW);
        btnX = findViewById(R.id.btnX);
        btnY = findViewById(R.id.btnY);
        btnZ = findViewById(R.id.btnZ);
        btnfinish = findViewById(R.id.btnFinish);
        btnNext = findViewById(R.id.btnNext);


        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getText((Button) v);

            }
        };

        btnA.setOnClickListener(onClickListener);
        btnB.setOnClickListener(onClickListener);
        btnC.setOnClickListener(onClickListener);
        btnD.setOnClickListener(onClickListener);
        btnE.setOnClickListener(onClickListener);
        btnF.setOnClickListener(onClickListener);
        btnG.setOnClickListener(onClickListener);
        btnH.setOnClickListener(onClickListener);
        btnI.setOnClickListener(onClickListener);
        btnJ.setOnClickListener(onClickListener);
        btnK.setOnClickListener(onClickListener);
        btnL.setOnClickListener(onClickListener);
        btnM.setOnClickListener(onClickListener);
        btnN.setOnClickListener(onClickListener);
        btnO.setOnClickListener(onClickListener);
        btnP.setOnClickListener(onClickListener);
        btnQ.setOnClickListener(onClickListener);
        btnR.setOnClickListener(onClickListener);
        btnS.setOnClickListener(onClickListener);
        btnT.setOnClickListener(onClickListener);
        btnU.setOnClickListener(onClickListener);
        btnV.setOnClickListener(onClickListener);
        btnW.setOnClickListener(onClickListener);
        btnX.setOnClickListener(onClickListener);
        btnY.setOnClickListener(onClickListener);
        btnZ.setOnClickListener(onClickListener);

        choseWord();

        btnfinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String wordLowercase = selecetedWord.toLowerCase();
                String wordLowercase2 = selecetedWord2.toLowerCase();

                Log.d("log", "selectedword: |" + selecetedWord + "|" + "      wordlowercase: |" + wordLowercase + "|");
                Log.d("log", "selectedword2: |" + selecetedWord2 + "|" + "     wordlowercase2: |" + wordLowercase2 + "|");
                Log.d("log", "finalword: |" + finalWord + "|");

                if (finalWord.equals("")) {
                    Toast.makeText(MainActivity.this, "Enter word", Toast.LENGTH_SHORT).show();

                } else {
                    if (finalWord.equals(wordLowercase)) {
                        txtDash.setText(finalWord);

                    } else if (finalWord.equals(wordLowercase2)) {
                        txtSecondWord.setText(finalWord);

                    } else {
                        Toast.makeText(MainActivity.this, "Wrong answer", Toast.LENGTH_SHORT).show();

                    }

                    finalWord = "";
                    if (!txtDash.getText().toString().contains("-")) {
                        if (!txtSecondWord.getText().toString().contains("-")) {
                            btnfinish.setEnabled(false);
                            btnfinish.setBackgroundColor(getResources().getColor(R.color.disablebuttoncolor));
                            Toast.makeText(MainActivity.this, "Well done", Toast.LENGTH_SHORT).show();

                        }

                    }


                }

            }

        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextQuestion();
                btnfinish.setEnabled(true);
                btnfinish.setBackgroundColor(getResources().getColor(R.color.enablebuttoncolor));
            }
        });

    }

    private void nextQuestion() {

        if (!txtDash.getText().toString().contains("-")) {
            if (!txtSecondWord.getText().toString().contains("-")) {
                finalWord = "";
                choseWord();

            }
        } else if (txtDash.getText().toString().contains("-") || txtSecondWord.getText().toString().contains("-")) {
            Toast.makeText(this, "please enter the all words", Toast.LENGTH_SHORT).show();
        }
    }


    private String selectWord() {
        String category = getIntent().getStringExtra("category");
        String[] words;

        if (category.equals("food")) {
            words = food;
        } else if (category.equals("car")) {
            words = car;
        } else {
            words = animal;
        }

        if (words.length == 0) {
            return null;
        }

        int randomeIndex = (int) (Math.random() * words.length);
        String word = words[randomeIndex];

        String[] newWords = new String[words.length - 1];

        int newPosition = 0;

        for (int i = 0; i < words.length; i++) {
            if (i == randomeIndex) {
                continue;
            }

            newWords[newPosition] = words[i];
            newPosition++;
        }

        if (category.equals("food")) {
            food = newWords;
        } else if (category.equals("car")) {
            car = newWords;
        } else {
            animal = newWords;
        }

        return word;
    }

    private String dashWord(String string) {
        String dash = "";
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) != ' ') {
                dash += "-";
            } else {
                dash += " ";
            }

        }
        return dash;
    }


    private String getIdString(View view) {
        String id = view.getResources().getResourceEntryName(view.getId());
        return id;
    }

    private void initButtonVisibility(String word, String word2, Button btn) {
        String id = getIdString(btn);
        String letter = id.replace("btn", "").toLowerCase();

        int visibility = View.INVISIBLE;

        if (word.contains(letter)) {
            visibility = View.VISIBLE;
        } else if (word2.contains(letter)) {
            visibility = View.VISIBLE;

        }

        btn.setVisibility(visibility);
    }

    private void getText(Button button) {
        String getText = button.getText().toString();
        finalWord += getText;

    }

    private void choseWord() {

        selecetedWord = selectWord();

        if (selecetedWord == null) {
            Toast.makeText(this, "Finish game", Toast.LENGTH_SHORT).show();
            return;
        }

        selecetedWord2 = selectWord();

        dash = dashWord(selecetedWord);
        txtDash.setText(dash);
        dash2 = dashWord(selecetedWord2);
        txtSecondWord.setText(dash2);


        final String word = selecetedWord.toLowerCase();
        final String word2 = selecetedWord2.toLowerCase();


        initButtonVisibility(word, word2, btnA);
        initButtonVisibility(word, word2, btnB);
        initButtonVisibility(word, word2, btnC);
        initButtonVisibility(word, word2, btnE);
        initButtonVisibility(word, word2, btnD);
        initButtonVisibility(word, word2, btnF);
        initButtonVisibility(word, word2, btnG);
        initButtonVisibility(word, word2, btnH);
        initButtonVisibility(word, word2, btnI);
        initButtonVisibility(word, word2, btnJ);
        initButtonVisibility(word, word2, btnK);
        initButtonVisibility(word, word2, btnL);
        initButtonVisibility(word, word2, btnM);
        initButtonVisibility(word, word2, btnN);
        initButtonVisibility(word, word2, btnO);
        initButtonVisibility(word, word2, btnP);
        initButtonVisibility(word, word2, btnQ);
        initButtonVisibility(word, word2, btnR);
        initButtonVisibility(word, word2, btnS);
        initButtonVisibility(word, word2, btnT);
        initButtonVisibility(word, word2, btnU);
        initButtonVisibility(word, word2, btnV);
        initButtonVisibility(word, word2, btnW);
        initButtonVisibility(word, word2, btnX);
        initButtonVisibility(word, word2, btnY);
        initButtonVisibility(word, word2, btnZ);

    }


}

